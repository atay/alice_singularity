#!/bin/bash

# Author: Serhat Atay
# atay@compeng.uni-frankfurt.de
#
# Usage: sudo ./singularity_test.sh -t [<environment tag>] -o [<output path>]
# superuser is needed to build a singularity image. if --fakeroot option can be used to build an image, then program may run as normaluser. Needs testing
#
# environment tag: <package_name>/<version> (mandatory field)
# for example: AliPhysics/vAN-20201018_ROOT6-1
#
# Program checks the existence of /cvmfs/alice.cern.ch/bin folder if cvmfs alice repository is mounted
# Program checks "singularity --version" command if the singularity is installed
#


# in case output is not specified, work directory will be current directory
workd=$(pwd)

# loop for arguments
while test $# -gt 0; do
  case "$1" in

    -h|--help)
      echo "options:"
      echo "-h, --help                        show brief help"
      echo "-y, --yes                         Yes to folder clean up"
      echo "-t, --tag <tag>                   specify a valid environment tag"
      echo "-o, --output <path>               specify a working directory, default: current directory"
      exit 0
      ;;

    -y|--yes)
      clean="y"
      shift
      ;;

    -t|--tag)
      shift
      if test $# -gt 0; then
        env=$1
      else
        echo "No valid environment tag specified. Exiting."
        exit 0
      fi
      shift
      ;;

    -o|--output)
      shift
      if test $# -gt 0; then
        workd=$1
      else
        echo "No valid output path specified. Using current directory as output."
      fi
      shift
      ;;

    *)
      shift
      echo "No valid arguments. Ignoring the invalid argument."
      shift
      ;;
  esac
done

# checking if environment tag is specified. Program will not continue without an environment tag
if [ -z $env ]
then
  echo "Please specify an environment tag using -t or --tag. Exiting."
  exit 0
fi

# checking cvmfs mount
if [[ ! -d /cvmfs/alice.cern.ch/bin/ ]]
then
  echo "cvmfs/alice.cern.ch is not mounted. Exiting."
  exit 0
fi

# checking singulaity installation on the system
if [[ $(singularity --version) != "singularity version"* ]]
then
  echo "singulariy is not installed on the system. Exiting."
  exit 0
fi

# checking if the output path ends with "/". if so, it is removed
# $workdir will be used as working directory
i=$((${#workd}-1))
if [[ ${workd:$i:$(($i+1))} == '/' ]]
then
  workdir=${workd:0:$i}
else
  workdir=$workd
fi

# $env is the environment tag in the format <package_name>/<version>
# replacing "/" characters in an environment tag with "-" character. "/" character will yield nested folder creation
# $environment is used for naming the folders and files
environment=${env/\//-}

# environment specific working directory under $workdir/. It will create a new folder for each tag under $workdir directory
scratchdir=${workdir}/${environment}

# environment specific working directory where everything is going to happen
if [[ ! -d "${scratchdir}" ]] #check if the directory exists. if not it creates
then
  echo ${scratchdir} " does not exist. Creating the parent folders."
  mkdir -p ${scratchdir}

elif [[ "$(ls -A ${scratchdir}/)" ]] && [[ -z $clean ]] #check if the folder is empty. if not ask for deletion
then
  echo ${scratchdir} " is not empty. Clean? y/n:"
  read clean
  if [[ $clean == "y" ]] || [[ $clean == "Y" ]] || [[ $clean == "yes" ]] || [[ $clean == "YES" ]] #asks for user input to clean the directory
  then
    rm -r ${scratchdir}/*
    echo ${scratchdir} " content is deleted."
  else                                                                          #do not clean the directory and exits
    echo ${scratchdir} " content is not deleted. Exiting."
    exit 0
  fi

elif [[ "$(ls -A ${scratchdir}/)" ]] && [[ ! -z $clean ]] #check if the folder is empty. if "-y" is typed deletes the content
then
  rm -r ${scratchdir}/*
  echo ${scratchdir} " content is deleted."
fi

#creation of a bash file to fetch $LD_LIBRARY_PATH into $scratchdir/$environment_LD_LIBRARY_PATH.txt file
cat > ${scratchdir}/${environment}_modules.sh <<EOF
#!/bin/bash
sed 's/:/\n/g' <<< "\$LD_LIBRARY_PATH" | tee SCRATCHDIR/ENVIRONMENT_LD_LIBRARY_PATH.txt
EOF
sed -i "s|SCRATCHDIR|${scratchdir}|g" ${scratchdir}/${environment}_modules.sh
sed -i "s|ENVIRONMENT|${environment}|g" ${scratchdir}/${environment}_modules.sh
chmod 755 ${scratchdir}/${environment}_modules.sh

#running modules.sh file inside a singularity base image with the environment tag. GSI singularity base image is used from /cvmfs repository
singularity exec -B $(pwd) -B /cvmfs -B $scratchdir /cvmfs/alice.cern.ch/containers/fs/singularity/centos7 /cvmfs/alice.cern.ch/bin/alienv setenv $env -c ${scratchdir}/${environment}_modules.sh

#loop to convert LD_LIBRARY_PATH entries to absolute paths of packages
while IFS='' read -r line || [[ -n "$line" ]]; do
  if [[ "$line" == *"singularity"* ]]; then
    continue
  fi

  array=();
  delimiter=/Packages/
  s=$line$delimiter
  while [[ $s ]]; do
    array+=( "${s%%"$delimiter"*}" );
    s=${s#*"$delimiter"};
  done;
  path="${array[0]}/Packages"
  x=${array[1]}
  unset array

  array=();
  delimiter=/
  s=$x$delimiter
  while [[ $s ]]; do
    array+=( "${s%%"$delimiter"*}" );
    s=${s#*"$delimiter"};
  done;
  package=${array[0]}
  version=${array[1]}
  unset array

  #echo ""
  #echo $path     #prefix of the package under /cvmfs repository
  #echo $package  #name of the package
  #echo $version  #version of the package

  #creating relevant folders and copying packages by preserving symlinks. Symlinks need to be preserved or else environment cannot be set inside the image.
  mkdir -p ${scratchdir}${path}/${package}/
  cp --preserve=links -rvf ${path}/${package}/${version} ${scratchdir}${path}/${package}/

done < ${scratchdir}/${environment}_LD_LIBRARY_PATH.txt

#all modules are copied since they don't book much space. Only the needed module files can be copied later. At the moment, all module files are copied for simplicity.
mkdir -p ${scratchdir}/cvmfs/alice.cern.ch/x86_64-2.6-gnu-4.1.2/Modules
cp --preserve=links -rvf /cvmfs/alice.cern.ch/x86_64-2.6-gnu-4.1.2/Modules/* ${scratchdir}/cvmfs/alice.cern.ch/x86_64-2.6-gnu-4.1.2/Modules/
mkdir -p ${scratchdir}/cvmfs/alice.cern.ch/el6-x86_64/Modules/modulefiles
cp --preserve=links -rvf /cvmfs/alice.cern.ch/el6-x86_64/Modules/modulefiles/* ${scratchdir}/cvmfs/alice.cern.ch/el6-x86_64/Modules/modulefiles/
mkdir -p ${scratchdir}/cvmfs/alice.cern.ch/el7-x86_64/Modules/modulefiles
cp --preserve=links -rvf /cvmfs/alice.cern.ch/el7-x86_64/Modules/modulefiles/* ${scratchdir}/cvmfs/alice.cern.ch/el7-x86_64/Modules/modulefiles/

#/etc and /bin folders are needed to set the environment
cp --preserve=links -rvf /cvmfs/alice.cern.ch/etc ${scratchdir}/cvmfs/alice.cern.ch/
cp --preserve=links -rvf /cvmfs/alice.cern.ch/bin ${scratchdir}/cvmfs/alice.cern.ch/

#going into the working directory ans start compressing of the copied respository. -cp arguments is important to preserve symlinks
cd $scratchdir
tar -cpzvf cvmfs.tar.gz cvmfs/

#creating the .def file for the final image. It uses the same docker base which is used to fetch environment packages above. It can be thought as the upgraded version of Raffaele's base image
cat > ${environment}.def <<EOF
Bootstrap: docker
From: centos:centos7
IncludeCmd: no

%files
	cvmfs.tar.gz /

%post
  rpmdb --rebuilddb && yum clean all && rm -rf /var/cache/yum
	yum update -y
	yum -y  install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
	yum install -y attr autoconf automake avahi-compat-libdns_sd-devel bc bind-export-libs bind-libs bind-libs-lite bind-utils binutils bison bzip2-devel cmake compat-libgfortran-41 compat-libstdc++-33 e2fsprogs e2fsprogs-libs environment-modules fftw-devel file-devel flex gcc gcc-c++ gcc-gfortran git glew-devel glibc-devel glibc-static gmp-devel graphviz-devel java-1.7.0-openjdk libcurl-devel libpng-devel libtool libX11-devel libXext-devel libXft-devel libxml2-devel libxml2-static libXmu libXpm-devel libyaml-devel mesa-libGL-devel mesa-libGLU-devel motif-devel mpfr-devel mysql-devel ncurses-devel openldap-devel openssl-devel openssl-static pciutils-devel pcre-devel perl-ExtUtils-Embed perl-libwww-perl protobuf-devel python-devel readline-devel redhat-lsb rpm-build swig tcl tcsh texinfo tk-devel unzip uuid-devel wget which zip zlib-devel zlib-static zsh
	#yum -y install https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest.noarch.rpm
  #yum -y install cvmfs cvmfs-config-default
	yum clean all
	yum update -y
	tar -xphzvf cvmfs.tar.gz -C /
	rm /cvmfs.tar.gz

%labels
Author Serhat Atay
EOF

#finally building the image
singularity build ${environment}.sif ${environment}.def
