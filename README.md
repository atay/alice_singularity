# Singularity contaiber for ALICE environment

## Description
A shell script to fetch the binaries from alice.cern.ch cvmfs repository for a specific environment tag and builds a singularity image for a standalone environment container

## Prerequisities
- singularity
- cvmfs alice.cern.ch mount under /cvmfs/alice.cern.ch
- linux OS (tested for centos7, other operating systems need to be tested)

## Usage
- git clone https://git.gsi.de/atay/alice_singularity
- cd alice_singularity
- sudo ./singularity_alice.sh -t <environment_tag> -o <working_directory> -y
    - -t/--tag              
        - specify a valid environment tag, for example "AliPhysics/vAN-20201018_ROOT6-1"
    - -o/--output           
        - specify a working directory, default: current directory
    - -y/--yes              
        - Yes to folder clean up. it will clean the content of working directory. It is not mandatory, however it will be asked by the program if not specified here
    - -h/--help             
        - displays help

## Procedure explanation
- The shell script checks for arguments passed by the command
    - if no environment tag is specified, it exits
    - if alice.cern.ch cvmfs repository is not mounted, it exists
    - if singularity is not installed, it exits
    - if no output is specified, it uses the current directory as the working directory
- It checks the **<working_directory>/<environment_tag>** folder
    - if not exists, it creates
    - if exists, clean the content of the folder. It requires an authentication from the user and the authentication can also be given by -y/--yes tag within the command
    - if exists and the user doesn't authenticate to clean the folder content, it exits
- It creates "**<environment_tag>_modules.sh**" file to fetch $LD_LIBRARY_PATH from the environment
- It executes the "<environment_tag>_modules.sh" within the singularity image "/cvmfs/alice.cern.ch/containers/fs/singularity/centos7" and fetch $LD_LIBRARY_PATH to "**<environment_tag>_LD_LIBRARY_PATH.txt**"
- It loops over entries in "<environment_tag>_LD_LIBRARY_PATH.txt" file and detects package name and versions together with its path prefix. After it is done, it creates a copy of the binary packages under **<working_directory>/<environment_tag>/cvmfs** by preserving the symlinks. This folder represents a subresposiory of alice.cern.ch cvmfs repo for a specific environment tag
- Fot simplicity, it copies all the modules files in the repo since they are just text files. Modules files are also needed to append binary paths to $LD_LIBRARY_PATH
- It then compress the <working_directory>/<environment_tag>/cvmfs to be able copy into the image during build. 
- It is now ready to build the image. It creates a "**<environment_tag>.def**" file.
- Final stage is to build the image using "<environment_tag>.def" file.
## Notes
- The image file can be found under "**<working_directory>/<environment_tag>/<environment_tag>.sif**"
- Image can be run with singularity "**singularity run -B /<some_path> <image.sif>**"
    - Do not bind mount cvmfs repository with "-B /cvmfs" tag. This then will bind the host /cvmfs folder to the container /cvmfs folder. This can conflicts and prevents working. The idea is to use this container where cvmfs repository is not accessible. If you already have cvmfs mount already, you don't need container. 
    - It will create a centos7 container with a virtual copy of alice.cern.ch cvmfs repository. However it only includes binaries for only the given environment.
    - The environment can be initiated as usual: "**/cvmfs/alice.cern.ch/bin/alienv enter <environment_tag>**"
        - If you have the image and are able to run it, then you don't need anything else to initiate the environment. No cvmfs mount, no ALICE account is needed. It is also possible to initiate the environment even without internet connection since you have all the binaries locally. 
        - It is important to know that this only provides the environment, not the data or any kind of analysis code. User needs to provide it him/herself